package com.app_for_testing_students.security;

import com.app_for_testing_students.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;

@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

//    @PersistenceContext
//    private EntityManager em;

    @Autowired
    UserService userService;

    @Override
    public org.springframework.security.core.userdetails.UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

//        CriteriaBuilder cb = em.getCriteriaBuilder();
//        CriteriaQuery<User> query = cb.createQuery(User.class);
//        Root<User> root = query.from(User.class);
//        query.where(cb.equal(cb.lower(root.get("email")), s.toLowerCase()));


        try {
//            return new UserDetails(em.createQuery(query).getSingleResult());
            return new UserDetails(userService.findUserByEmail(s));
        } catch (NoResultException e) {
            return null;
        }
    }

}
