package com.app_for_testing_students.service;

import com.app_for_testing_students.entity.User;

import java.util.Set;

public interface UserService {

    User saveUser(User user);

    User findUserByEmail(String email);

    User registerNewUser(User newUser);

    User getCurrentUser();

    Set<User> findByOrg();

    User findUserById(int userId);

    User assignRoles(User userChanged);

    User leftOrganisation(User user);


}
