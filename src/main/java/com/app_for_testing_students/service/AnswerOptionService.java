package com.app_for_testing_students.service;

import com.app_for_testing_students.entity.AnswerOption;

public interface AnswerOptionService {
    AnswerOption saveAnswerOption(AnswerOption ansOpt);

    AnswerOption findById(int ansOptId);

}
