package com.app_for_testing_students.service.impl;

import com.app_for_testing_students.entity.Task;
import com.app_for_testing_students.exception.BuisnessException;
import com.app_for_testing_students.repository.TaskRepository;
import com.app_for_testing_students.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    TaskRepository taskRepository;

    @Override
    public Task save(Task task) {
        if (task.getTest() == null) {
            throw new BuisnessException("incorrect test id");
        }
        return taskRepository.save(task);
    }


    @Override
    public Task findById(int id) {
        return taskRepository.findById(id);
    }


    @Override
    public void deleteTask(Task task) {

        taskRepository.delete(task);
    }
}