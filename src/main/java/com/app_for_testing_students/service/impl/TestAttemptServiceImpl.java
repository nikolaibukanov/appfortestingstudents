package com.app_for_testing_students.service.impl;

import com.app_for_testing_students.entity.TestAttempt;
import com.app_for_testing_students.entity.User;
import com.app_for_testing_students.exception.BuisnessException;
import com.app_for_testing_students.repository.TestAttemptRepository;
import com.app_for_testing_students.repository.TestRepository;
import com.app_for_testing_students.service.TestAttemptService;
import com.app_for_testing_students.service.TestService;
import com.app_for_testing_students.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Set;

@Service
public class TestAttemptServiceImpl implements TestAttemptService {

    @Autowired
    UserService userService;

    @Autowired
    TestService testService;

    @Autowired
    TestAttemptRepository testAttemptRepository;

    @Autowired
    TestRepository testRepository;

    @Transactional
    @Override
    public TestAttempt createAttempt(TestAttempt attempt) {

        User user = userService.getCurrentUser();
        attempt.setStudent(user);

        int attemptsNumber = testAttemptRepository.getAttemptNumber(attempt.getTest().getId(),
                attempt.getStudent().getId()) + 1;

        if (attemptsNumber > attempt.getTest().getMaxAttempts()) {
            throw new BuisnessException("Limit of attempts is exhausted");
        }

        attempt.setAttemptNumber(attemptsNumber);
        return testAttemptRepository.save(attempt);
    }

    @Override
    public Set<TestAttempt> getTestAttempts(int testId) {
        User user = userService.getCurrentUser();
        return testAttemptRepository.getStudentTestAttempts(user.getId(), testId);
    }

    @Override
    public TestAttempt findById(int attemptId) {

        return testAttemptRepository.findById(attemptId);
    }

    @Override
    public Set<TestAttempt> getTestManagerTestAttempts(int testId) {
        return testAttemptRepository.findByTestManagerId(testRepository.findById(testId).getManagerId().getId());
    }
}
