package com.app_for_testing_students.service.impl;

import com.app_for_testing_students.entity.AnswerOption;
import com.app_for_testing_students.entity.StudentAnswer;
import com.app_for_testing_students.entity.TestAttempt;
import com.app_for_testing_students.exception.BuisnessException;
import com.app_for_testing_students.repository.StudentAnswerRepository;
import com.app_for_testing_students.service.StudentAnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentAnswerServiceImpl implements StudentAnswerService {

    @Autowired
    StudentAnswerRepository studentAnswerRepository;

    @Override
    public StudentAnswer createStudentAnswer(AnswerOption ansOpt, TestAttempt attempt) {
        if (studentAnswerRepository.findStudentAnswerForTask(attempt.getId(), ansOpt.getTask().getId()) > 0) {
            throw new BuisnessException("you've already answered this question");
        }
        StudentAnswer answer = new StudentAnswer();
        answer.setAttempt(attempt);
        answer.setAnswerOption(ansOpt);
        answer.setCorrect(ansOpt.isCorrect());
        return studentAnswerRepository.save(answer);
    }
}
