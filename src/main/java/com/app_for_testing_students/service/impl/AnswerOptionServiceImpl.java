package com.app_for_testing_students.service.impl;

import com.app_for_testing_students.entity.AnswerOption;
import com.app_for_testing_students.repository.AnswerOptionRepository;
import com.app_for_testing_students.service.AnswerOptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AnswerOptionServiceImpl implements AnswerOptionService {

    @Autowired
    AnswerOptionRepository ansOptRepository;


    @Override
    public AnswerOption saveAnswerOption(AnswerOption ansOpt) {
        return ansOptRepository.save(ansOpt);
    }

    @Override
    public AnswerOption findById(int ansOptId) {
        return ansOptRepository.findById(ansOptId);
    }
}
