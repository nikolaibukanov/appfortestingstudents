package com.app_for_testing_students.service;

import com.app_for_testing_students.entity.AnswerOption;
import com.app_for_testing_students.entity.StudentAnswer;
import com.app_for_testing_students.entity.TestAttempt;

public interface StudentAnswerService {


    StudentAnswer createStudentAnswer(AnswerOption ansOpt, TestAttempt attempt);
}
