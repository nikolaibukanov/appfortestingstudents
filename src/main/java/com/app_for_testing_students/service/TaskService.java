package com.app_for_testing_students.service;

import com.app_for_testing_students.entity.Task;

public interface TaskService {

    Task save(Task task);

    Task findById(int id);

    void deleteTask(Task task);
}

