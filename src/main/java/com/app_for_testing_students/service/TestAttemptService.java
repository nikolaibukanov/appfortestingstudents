package com.app_for_testing_students.service;

import com.app_for_testing_students.entity.TestAttempt;

import java.util.Set;

public interface TestAttemptService {

    TestAttempt createAttempt(TestAttempt attempt);

    Set<TestAttempt> getTestAttempts(int testId);

    TestAttempt findById(int attemptId);

    Set<TestAttempt> getTestManagerTestAttempts(int testId);
}
