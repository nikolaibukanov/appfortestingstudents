package com.app_for_testing_students.service;


import com.app_for_testing_students.entity.Group;
import com.app_for_testing_students.entity.Test;

import java.util.Set;

public interface TestService {
    Test createTest(Test test);

    Test findById(int testId);

    void deleteTest(Test test);

    Set<Test> findAllOrgTests(String token);

    Test assignTest(Group group, Test test);

    Set<Test> findAllUserTests();
}
