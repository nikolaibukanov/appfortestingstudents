package com.app_for_testing_students.controller;

import com.app_for_testing_students.entity.AnswerOption;
import com.app_for_testing_students.entity.Task;
import com.app_for_testing_students.service.AnswerOptionService;
import com.app_for_testing_students.service.TaskService;
import com.app_for_testing_students.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "api/organisation/tests/{testId}/tasks/{taskId}/answerOptions")
public class AnswerOptionRestController {

    @Autowired
    TaskService taskService;

    @Autowired
    AnswerOptionService answerOptionService;

    @Autowired
    UserService userService;

    @PreAuthorize("isTestManager()")
    @PostMapping
    public ResponseEntity<AnswerOption> createAnswerOption(@PathVariable("testId") int testId,
                                                           @PathVariable("taskId") int taskId,
                                                           @RequestBody AnswerOption ansOpt) {
        Task task = taskService.findById(taskId);
        if (task == null ||
                task.getTest().getId() != testId || ansOpt == null ||
                task.getTest().getManagerId().getId() != userService.getCurrentUser().getId()) {
            return ResponseEntity.notFound().build();
        }
        ansOpt.setTask(task);
        return ResponseEntity.ok(answerOptionService.saveAnswerOption(ansOpt));
    }

    @PreAuthorize("isOrganisationOwner() || isTestManager() || isStudent()")
    @GetMapping(value = "/{ansOptId}")
    public ResponseEntity<AnswerOption> showAnswerOption(@PathVariable("testId") int testId,
                                                         @PathVariable("taskId") int taskId,
                                                         @PathVariable("ansOptId") int ansOptId) {
        Task task = taskService.findById(taskId);
        if (task == null ||
                task.getTest().getId() != testId ||
                !task.getAnswerOptions().contains(answerOptionService.findById(ansOptId))) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(answerOptionService.findById(ansOptId));
    }
}

