package com.app_for_testing_students.controller;


import com.app_for_testing_students.entity.Test;
import com.app_for_testing_students.entity.TestAttempt;
import com.app_for_testing_students.entity.User;
import com.app_for_testing_students.repository.RoleRepository;
import com.app_for_testing_students.service.TestAttemptService;
import com.app_for_testing_students.service.TestService;
import com.app_for_testing_students.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping("/api/test/{testId}/attempts")
public class TestAttemptRestController {

    @Autowired
    TestAttemptService testAttemptService;

    @Autowired
    TestService testService;

    @Autowired
    UserService userService;

    @Autowired
    RoleRepository roleRepository;

    @PreAuthorize("isStudent()")
    @PostMapping
    public ResponseEntity<TestAttempt> createAttempt(@PathVariable("testId") int testId) {
        Test test = testService.findById(testId);
        if (test == null ||
                !userService.getCurrentUser().getGroup().getTests().contains(test)) {
            return ResponseEntity.notFound().build();
        }
        TestAttempt attempt = new TestAttempt();
        attempt.setTest(test);
        attempt = testAttemptService.createAttempt(attempt);
        attempt.getTest().setTasks(null);
        attempt.getStudent().setUserRoles(null);
        attempt.getStudent().setPassword(null);
        return ResponseEntity.ok(attempt);
    }

    @PreAuthorize("isStudent() || isTestManager()")
    @GetMapping
    public ResponseEntity<Set<TestAttempt>> showAttempts(@PathVariable("testId") int testId) {
        User user = userService.getCurrentUser();
        Set<TestAttempt> testAttempts;
        if (user.getUserRoles().contains(roleRepository.findByName("STUDENT"))) {

            testAttempts = testAttemptService.getTestAttempts(testId);
        }
        else {
            testAttempts = testAttemptService.getTestManagerTestAttempts(testId);
        }
        return ResponseEntity.ok(testAttempts);
    }

}
