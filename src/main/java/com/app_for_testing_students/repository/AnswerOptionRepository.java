package com.app_for_testing_students.repository;

import com.app_for_testing_students.entity.AnswerOption;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnswerOptionRepository extends JpaRepository<AnswerOption, Integer> {
    AnswerOption findById(int ansOptId);
}
