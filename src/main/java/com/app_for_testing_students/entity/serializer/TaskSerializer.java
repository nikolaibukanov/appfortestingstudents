package com.app_for_testing_students.entity.serializer;

import com.app_for_testing_students.entity.AnswerOption;
import com.app_for_testing_students.entity.Task;
import com.app_for_testing_students.service.UserService;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class TaskSerializer extends JsonSerializer{

    @Autowired
    UserService userService;

    @Override
    public void serialize(Object o, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeObjectField("taskId", ((Task)o).getId());
        jsonGenerator.writeObjectField("task Text", ((Task)o).getTaskText());


            jsonGenerator.writeArrayFieldStart("answer options");
//            jsonGenerator.writeStartArray();
            for (AnswerOption ansOpt : ((Task)o).getAnswerOptions()) {
                jsonGenerator.writeStartObject();
                jsonGenerator.writeObjectField("answer option id", ansOpt.getId());
                jsonGenerator.writeObjectField("answer option", ansOpt.getAnswer());
                jsonGenerator.writeEndObject();
            }
            jsonGenerator.writeEndArray();

        jsonGenerator.writeEndObject();
    }
}
