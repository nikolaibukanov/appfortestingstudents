package com.app_for_testing_students.entity.serializer;

import com.app_for_testing_students.entity.Test;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class TestSerializer extends JsonSerializer {



    @Override
    public void serialize(Object o, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeObjectField("testId", ((Test)o).getId());
        jsonGenerator.writeObjectField("name", ((Test)o).getName());
        jsonGenerator.writeEndObject();
    }
}
